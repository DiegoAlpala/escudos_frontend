import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AsignacionEscudosComponent } from './components/paginas/asignacion-escudos/asignacion-escudos.component';
import { ConsultasComponent } from './components/paginas/consultas/consultas.component';
import { MaterialesComponent } from './components/paginas/materiales/materiales.component';
import { ObrasConsultaComponent } from './components/paginas/obras-consulta/obras-consulta.component';
import { ObrasEditarComponent } from './components/paginas/obras/obras-editar/obras-editar.component';
import { ObrasComponent } from './components/paginas/obras/obras.component';
import { ConfiguracionComponent } from './components/paginas/sistema/configuracion/configuracion.component';
import { UsuarioComponent } from './components/paginas/sistema/usuario/usuario.component';
import { SocioTecnicoComponent } from './components/paginas/socio-tecnico/socio-tecnico.component';
import { VistaPublicaQrComponent } from './components/paginas/vista-publica-qr/vista-publica-qr.component';
import { GuardService } from './_servicio/guard.service';

const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full'},
    { path: 'login', component: LoginComponent},
    { path: 'home', component: HomeComponent, canActivate: [GuardService] },
    { path: 'usuario', component: UsuarioComponent, canActivate: [GuardService] },
    { path: 'socio_tecnico', component: SocioTecnicoComponent, canActivate: [GuardService] },
    { path: 'obras', component: ObrasComponent, canActivate: [GuardService], children :[
        { path: 'edicion/:id', component: ObrasEditarComponent }
    ] },
    { path: 'ver_informacion_qr/:serie', component: VistaPublicaQrComponent },
    { path: 'asignacion_garantia', component: AsignacionEscudosComponent, canActivate: [GuardService] },
    { path: 'consulta_qr', component: ObrasConsultaComponent, canActivate: [GuardService] },
    { path: 'consulta', component: ConsultasComponent, canActivate: [GuardService] },
    { path: 'variables', component: ConfiguracionComponent, canActivate: [GuardService] },
    { path: 'materiales', component: MaterialesComponent, canActivate: [GuardService] },
]

@NgModule({

    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRountigModule { }
