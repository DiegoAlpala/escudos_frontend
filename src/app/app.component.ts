import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoginService } from './_servicio/login.service';
import { NotificarService } from './_servicio/notificar.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    constructor(
        public _loginService: LoginService,
        private _notificarService: NotificarService,
        private spinner: NgxSpinnerService) { }

    ngOnInit(): void {
        this._notificarService.loadingCambio.subscribe(data => {
            if (data) {
                this.spinner.show();
            } else {
                this.spinner.hide();
            }
        })

        this._notificarService.mensajeRequest.subscribe(data => {

            switch (data.tipo) {
                case 'error':
                    this._notificarService.mostrarMensajeError(data.detalle);
                    break;
                case 'success':
                    this._notificarService.mostrarMensajeExito(data.detalle);
                    break;
                case 'warning':
                    this._notificarService.mostrarMensajeAdvertencia(data.detalle);
                    break;
                case 'info':
                    this._notificarService.mostrarMensajeInformacion(data.detalle);
                    break;
            }
        })
    }
}
