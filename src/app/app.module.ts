import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MenuModule } from '@progress/kendo-angular-menu';
import { ExcelModule, GridModule } from '@progress/kendo-angular-grid';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { DropDownListModule, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { PopupModule } from '@progress/kendo-angular-popup';
import { InputsModule } from '@progress/kendo-angular-inputs';

import 'hammerjs';

import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { Blank1Component } from "./components/blank-1/blank-1.component";
import { NotificationModule } from '@progress/kendo-angular-notification';
import { AppRountigModule } from './app-routing-module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { JwtModule } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { ServerErrorsInterceptor } from './_util/server-errors-interceptor';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { SocioTecnicoComponent } from './components/paginas/socio-tecnico/socio-tecnico.component';
import { ObrasComponent } from './components/paginas/obras/obras.component';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { LabelModule } from '@progress/kendo-angular-label';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { SocioTecnicoRegistrarComponent } from './components/paginas/socio-tecnico/socio-tecnico-registrar/socio-tecnico-registrar.component';
import { VistaPublicaQrComponent } from './components/paginas/vista-publica-qr/vista-publica-qr.component';
import { ObrasEditarComponent } from './components/paginas/obras/obras-editar/obras-editar.component';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { AsignacionEscudosComponent } from './components/paginas/asignacion-escudos/asignacion-escudos.component';
import { AsignacionResultadoComponent } from './components/paginas/asignacion-escudos/asignacion-resultado/asignacion-resultado.component';
import { ActiveBadgeComponent } from './components/commons/active-badge/active-badge.component';
import { GarantiaTipoBadgeComponent } from './components/commons/garantia-tipo-badge/garantia-tipo-badge.component';
import { ObrasEditarSistemaComponent } from './components/paginas/obras/obras-editar-sistema/obras-editar-sistema.component';
import { ObrasEditarEscudosComponent } from './components/paginas/obras/obras-editar-escudos/obras-editar-escudos.component';
import { TooltipModule } from '@progress/kendo-angular-tooltip';
import { ListViewModule } from '@progress/kendo-angular-listview';
import { ObrasConsultaComponent } from './components/paginas/obras-consulta/obras-consulta.component';
import { EstadoBadgeComponent } from './components/commons/estado-badge/estado-badge.component';
import { UsuarioComponent } from './components/paginas/sistema/usuario/usuario.component';
import { UsuarioRegistrarComponent } from './components/paginas/sistema/usuario/usuario-registrar/usuario-registrar.component';
import { UsuarioResetPasswordComponent } from './components/paginas/sistema/usuario/usuario-reset-password/usuario-reset-password.component';
import { ConsultasComponent } from './components/paginas/consultas/consultas.component';
import { AgmCoreModule } from '@agm/core';
import { ConfiguracionComponent } from './components/paginas/sistema/configuracion/configuracion.component';
import { MaterialesComponent } from './components/paginas/materiales/materiales.component';





//Generamos la función para obtener el token almacenado en localsesionStorage
export function tokenGetter() {
    let tk = sessionStorage.getItem(environment.TOKEN_NAME);
    return tk != null ? tk : '';
}

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        Blank1Component,
        FooterComponent,
        LoginComponent,
        SocioTecnicoComponent,
        ObrasComponent,
        SocioTecnicoRegistrarComponent,
        VistaPublicaQrComponent,
        ObrasEditarComponent,
        AsignacionEscudosComponent,
        AsignacionResultadoComponent,
        ActiveBadgeComponent,
        GarantiaTipoBadgeComponent,
        ObrasEditarSistemaComponent,
        ObrasEditarEscudosComponent,
        ObrasConsultaComponent,
        EstadoBadgeComponent,
        UsuarioComponent,
        UsuarioRegistrarComponent,
        UsuarioResetPasswordComponent,
        ConsultasComponent,
        ConfiguracionComponent,
        MaterialesComponent
    ],
    entryComponents: [SocioTecnicoRegistrarComponent],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        MenuModule,
        BrowserAnimationsModule,
        GridModule,
        ChartsModule,
        AppRountigModule,
        HttpClientModule,
        DropDownsModule, PopupModule, InputsModule, NotificationModule,
        ButtonModule,
        LayoutModule,
        LabelModule,
        NgxSpinnerModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                allowedDomains: ["localhost:8080", "http://192.168.4.15:8069", "http://181.39.46.3:8069"]
                //allowedDomains: ["http://172.31.1.7:8069"], //Para Pruebas
                //allowedDomains: ["http://172.31.1.7:8082"], //Para Produccion
                //disallowedRoutes: ["http://localhost:8080:login/enviarcorreo"]
            }
        }),
        LayoutModule,
        LabelModule,
        DialogsModule,
        DropDownListModule,
        DateInputsModule,
        TooltipModule,
        ListViewModule,
        ExcelModule,
        AgmCoreModule.forRoot({
            // please get your own API key here:
            // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
            apiKey: 'AIzaSyDnxbmRGFm_wnMV-EBRKSq8k3Wt0vVYjes'
        })
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ServerErrorsInterceptor,
            multi: true,
        },
        { provide: LocationStrategy, useClass: HashLocationStrategy },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
