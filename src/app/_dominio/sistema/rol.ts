import { Menu } from "src/app/_dominio/sistema/menu";

export class Rol {
    id: number;
    nombre: string;
    descripcion: string;
    activo: boolean;
    detalleMenu: Menu[] = [];
}
