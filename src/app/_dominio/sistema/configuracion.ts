export class Configuracion {

    id: number;
    nombreConfiguracion: string;
    valorConfiguracion: string;
    descripcion: string;
}
