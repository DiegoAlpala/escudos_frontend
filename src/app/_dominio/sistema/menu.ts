
export class Menu {

    id: number;
    icono: string;
    nombre: string;
    url: string;
    subMenus: Menu[];
    orden: number;
}