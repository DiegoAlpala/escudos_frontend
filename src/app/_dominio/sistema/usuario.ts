import { Rol } from "./rol";

export class Usuario {

    id: number;
    nombreUsuario: string;
    nombreCompleto: string;
    correo: string;
    activo: boolean;
    rol: Rol;
    tipo: string;
    contrasena: string;
}
