import { EscudoGarantia } from "./escudoGarantia";
import { ImpermeabilizacionSistema } from "./impermeabilizacionSistema";
import { SocioTecnico } from "./socioTecnico";

export class Obra {
    id: number;
    anios: number;
    tipoGarantia: string;
    region: string;
    fechaInicioGarantia: string;
    longitud: number;
    latitud: number;
    producto: string;
    lote: string;
    dimension: number;
    nombrePropietario: string;
    razonSocial: string;
    serie: string;
    generadoQR: boolean;
    socio: SocioTecnico;
    ubicacionEscudo: string;
    sistemas: ImpermeabilizacionSistema[] = [];
    escudos: EscudoGarantia[] = [];
    estado: '';
    asignado: string;
    comentario: string;
    ciudad: string;

    nombreBroker: string;
    contactoBroker: string;
}
