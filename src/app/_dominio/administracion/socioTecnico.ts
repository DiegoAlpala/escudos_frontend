export class SocioTecnico {
	id: number;
	nombre: String;
	celular: String;
	telefono: string;
	correo: string;
	activo: boolean;
	codigoIdentificador: string;
	nombreContacto: string;
	telefonoContacto: string;
    comentario: string;
    enlace: string;
}
