export class ConsultaQR {
    id: number;
    socio: string;
    estado: string;
    tipoGarantia: string;
    serie: string;
}