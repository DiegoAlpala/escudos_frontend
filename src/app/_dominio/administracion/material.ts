import { MaterialCatalogo } from "./material-catalogo";

export class Material {
    id: number;
    nombreProducto: string;
    lote: string;
    cantidad: number;
    unidad: string;
    materialCatalogo: MaterialCatalogo;
}
