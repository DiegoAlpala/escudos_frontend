import { Material } from "./material";

export class ImpermeabilizacionSistema {
    id: number;
    nombre: string;
    tipo: string;
    tipoObra: string;
    dimension: number;
    materiales: Material[] = [];
}
