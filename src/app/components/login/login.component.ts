import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/_servicio/login.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';
import { MenuService } from 'src/app/_servicio/sistema/menu.service';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	busy = false;
    public username = '';
    public password = '';
    loginError = false;

	constructor(
		private route: ActivatedRoute,
        private router: Router,
        private _notificarService: NotificarService,
		private _loginService: LoginService,
		private _menuService: MenuService
	) { }

	ngOnInit(): void {
	}

	public login() {
		debugger
		if (!this.username || !this.password) {
            return;
        }
        this.busy = true;

		this._loginService.login(this.username, this.password).subscribe(data =>{
			this._notificarService.loadingCambio.next(false);
			sessionStorage.setItem(environment.TOKEN_NAME, data.access_token);
			this._menuService.listarPorRol().subscribe(data =>{
				this._notificarService.desactivarLoading();
				this._menuService.menuCambio.next(data);
			});
			this.router.navigate(['home']);
		})
	}

}
