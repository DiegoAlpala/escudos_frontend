import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogCloseResult, DialogRef, DialogService } from '@progress/kendo-angular-dialog';
import { forkJoin, Observable } from 'rxjs';
import { SocioTecnico } from 'src/app/_dominio/administracion/socioTecnico';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { SocioTecnicoService } from 'src/app/_servicio/adminitracion/socio-tecnico.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';
import { AsignacionResultadoComponent } from './asignacion-resultado/asignacion-resultado.component';

@Component({
    selector: 'app-asignacion-escudos',
    templateUrl: './asignacion-escudos.component.html',
    styleUrls: ['./asignacion-escudos.component.scss']
})
export class AsignacionEscudosComponent implements OnInit {

    public sociosLista: SocioTecnico[] = [];
    public tipoGarantiaLista: any[] = [];

    public formAsignacion = new FormGroup({
        'socio': new FormControl(0, Validators.required),
        'tipoGarantia': new FormControl('', Validators.required),
        'cantidad': new FormControl(0, Validators.required)
    });

    @ViewChild("containerConfirmacion", { read: ViewContainerRef })
    public containerConfirmacionRef: ViewContainerRef;
    @ViewChild("asignacionResultado", { read: ViewContainerRef })
    public asignacionResultadoRef: ViewContainerRef;

    constructor(
        private _notificarService: NotificarService,
        private _socioService: SocioTecnicoService,
        private _obraService: ObraService,
        private dialogService: DialogService,
    ) { }

    ngOnInit(): void {
        this.setearListas().subscribe(data => {
            this._notificarService.loadingCambio.next(false);
            this.sociosLista = data[0];
            this.tipoGarantiaLista = data[1];
        })
    }

    private setearListas(): Observable<any[]> {
        let socios = this._socioService.listarTodosActivos();
        let tipoGarantia = this._obraService.obtenerCatalogoTipoGarantia();
        return forkJoin([socios, tipoGarantia]);
    }

    private asignarEscudosGarantias() {
        const socio = this.formAsignacion.controls['socio'].value;
        const tipoGarantia = this.formAsignacion.controls['tipoGarantia'].value;
        const cantidad = this.formAsignacion.controls['cantidad'].value;

        this._obraService.asignarGarantias(socio, tipoGarantia, cantidad).subscribe(data => {
            this._notificarService.desactivarLoading();
            this.abrirAsignacionResultadoDialogo(data);
        })
    }

    public mostrarConfirmacion() {
        let mensaje: string = `¿Está seguro que asignar (${this.formAsignacion.controls['cantidad'].value}) escudos de garantía del tipo ${this.formAsignacion.controls['tipoGarantia'].value} ?`;
        const dialog: DialogRef = this.dialogService.open({
            appendTo: this.containerConfirmacionRef,
            title: 'Confirmación',
            content: mensaje,
            actions: [
                { text: 'No' },
                { text: 'Sí', primary: true }
            ],
            width: 450,
            height: 200,
            minWidth: 200
        });

        dialog.result.subscribe((result) => {
            if (result instanceof DialogCloseResult) {

            } else {
                if (result['text'] === 'Sí') {
                    this.asignarEscudosGarantias();
                }
            }
        });
    }

    private abrirAsignacionResultadoDialogo(data) {
        const dialogCobroResultado: DialogRef = this.dialogService.open({
            appendTo: this.asignacionResultadoRef,
            title: 'Información',
            content: AsignacionResultadoComponent,
            actions: [
                { text: 'Aceptar', primary: true }
            ],
            width: 500,
            maxHeight: 400,
            minWidth: 250
        });
        const facturaResultadoRegistro = dialogCobroResultado.content.instance;
        facturaResultadoRegistro.resultado = data;

        dialogCobroResultado.result.subscribe((result) => {
            if (result instanceof DialogCloseResult) {
                this.blanquearDatos();
            } else {
                if (result['text'] === 'Aceptar') {
                    this.blanquearDatos();
                }
            }
        });
    }

    private blanquearDatos(){
        this.formAsignacion.reset();
    }


}
