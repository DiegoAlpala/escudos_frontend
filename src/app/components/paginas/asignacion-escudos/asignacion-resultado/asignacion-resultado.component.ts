import { Component, OnInit } from '@angular/core';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';

@Component({
    selector: 'app-asignacion-resultado',
    templateUrl: './asignacion-resultado.component.html',
    styleUrls: ['./asignacion-resultado.component.scss']
})
export class AsignacionResultadoComponent implements OnInit {

    public resultado: any[] = [];

    constructor(
        private _notificarService: NotificarService,
        private _obraService: ObraService,
    ) { }

    ngOnInit(): void {
    }

    public descargarQR(item: any){
        this._obraService.generarQR(item.id).subscribe(data =>{
            this._notificarService.desactivarLoading();
            const file = new Blob([data], { type: 'image/png' });
            const fileURL = URL.createObjectURL(file);
            const a = document.createElement('a');
            a.href = fileURL;
            a.download = `${item.serie}`;
            a.click();
        })
    }

}
