import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { orderBy, process, SortDescriptor, State } from '@progress/kendo-data-query';
import { switchMap } from 'rxjs/operators';
import { MaterialCatalogo } from 'src/app/_dominio/administracion/material-catalogo';
import { MaterialCatalogoService } from 'src/app/_servicio/adminitracion/material-catalogo.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';

const createFormGroup = dataItem => new FormGroup({
    'id': new FormControl(dataItem.id),
    'nombre': new FormControl(dataItem.nombre, Validators.required),
    'activo': new FormControl(dataItem.activo, Validators.required)
});

@Component({
    selector: 'app-materiales',
    templateUrl: './materiales.component.html',
    styleUrls: ['./materiales.component.scss']
})
export class MaterialesComponent implements OnInit {

    public usuariosLista: MaterialCatalogo[] = [];
    public usuarioNuevo: boolean = true;
    public usuarioEditar: MaterialCatalogo;

    public gridUsuariosView: GridDataResult;

    public formGroup: FormGroup;
    private editedRowIndex: number;

    public state: State = {
        skip: 0,
        take: 10,
        sort: [],
        filter: {
            logic: 'and',
            filters: [{ field: 'nombre', operator: 'contains', value: '' }]
        }
    }
    constructor(
        private _notificarService: NotificarService,
        private _materialesCatalogoService: MaterialCatalogoService
    ) { }

    ngOnInit(): void {
        this._materialesCatalogoService.maerialCatalogoCambio.subscribe(data => {
            this.refrescarGrid(data);
        })
        this.listarUsuarios();
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridUsuariosView = process(this.usuariosLista, this.state);
    }

    private listarUsuarios() {
        this._materialesCatalogoService.listarTodos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this.refrescarGrid(data);
        })
    }

    private refrescarGrid(data: MaterialCatalogo[]) {
        this.usuariosLista = data;
        this.gridUsuariosView = process(data, this.state);
    }

    public editarMaterial(item: MaterialCatalogo) {
        this.usuarioEditar = item;

    }

    public crearMaterial() {
        this.usuarioEditar = null;
    }

    public addHandler({ sender }) {
        this.closeEditor(sender);
        this.formGroup = createFormGroup({
            'id': 0,
            'nombre': '',
            'activo': true,
        });
        sender.addRow(this.formGroup);
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);
        this.formGroup = createFormGroup(dataItem);
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, formGroup, isNew }): void {
        const sistema = formGroup.value;
        this.guardar(sistema);
        sender.closeRow(rowIndex);
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

    private guardar(sistema) {
        if (sistema.id === 0) {
            this._materialesCatalogoService.registrar(sistema).pipe(switchMap(() => {
                return this._materialesCatalogoService.listarTodos();
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Material agregado");
                this._materialesCatalogoService.maerialCatalogoCambio.next(data);
                this.refrescarGrid(data);
            });

        } else {
            this._materialesCatalogoService.modificar(sistema).pipe(switchMap(() => {
                return this._materialesCatalogoService.listarTodos();
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Material actualizado");
                this._materialesCatalogoService.maerialCatalogoCambio.next(data);
                this.refrescarGrid(data);
            });
        }
    }

    public sort: SortDescriptor[] = [
        {
            field: "nombre",
            dir: "asc",
        },
    ];

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.loadProducts();
    }

    private loadProducts(): void {
        this.gridUsuariosView = {
            data: orderBy(this.usuariosLista, this.sort),
            total: this.usuariosLista.length,
        };
    }


}
