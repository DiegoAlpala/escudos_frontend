const data=[
    {
        text: "5K Rollo",
        value: "5K Rollo"
    },
    {
        text: "Superk 2500",
        value: "Superk 2500"
    },
    {
        text: "Superk 3000 Rollo 10M2",
        value: "Superk 3000 Rollo 10M2"
    },
    {
        text: "Super K 3000 Anti-raíz",
        value: "Super K 3000 Anti-raíz"
    },
    {
        text: "Techofielt 1500 Rollo 20M2 (1 cara de arena)",
        value: "Techofielt 1500 Rollo 20M2 (1 cara de arena)"
    },
    {
        text: "Techofielt 2000",
        value: "Techofielt 2000"
    },
    {
        text: "Imperglass 3000 Verde",
        value: "Imperglass 3000 Verde"
    },
    {
        text: "Imperglass 3000 Rojo",
        value: "Imperglass 3000 Rojo"
    },
    {
        text: "Imperglass 3000 Negro",
        value: "Imperglass 3000 Negro"
    },
    {
        text: "Imperglass 3000 Tabaco",
        value: "Imperglass 3000 Tabaco"
    },
    {
        text: "Imperglass 4000 Verde",
        value: "Imperglass 4000 Verde"
    },
    {
        text: "Imperglass 4000 Rojo",
        value: "Imperglass 4000 Rojo"
    },
    {
        text: "Imperglass 4000 Negro",
        value: "Imperglass 4000 Negro"
    },
    {
        text: "Imperglass 4000 Tabaco",
        value: "Imperglass 4000 Tabaco"
    },
    {
        text: "Imperpol 3000 Verde",
        value: "Imperpol 3000 Verde"
    },
    {
        text: "Imperpol 3000 Rojo",
        value: "Imperpol 3000 Rojo"
    },
    {
        text: "Imperpol 3000 Negro",
        value: "Imperpol 3000 Negro"
    },
    {
        text: "Imperpol 3000 Tabaco",
        value: "Imperpol 3000 Tabaco"
    },
    {
        text: "Imperpol 3000 Gris",
        value: "Imperpol 3000 Gris"
    },
    {
        text: "Imperpol 4000 Verde",
        value: "Imperpol 4000 Verde"
    },
    {
        text: "Imperpol 4000 Rojo",
        value: "Imperpol 4000 Rojo"
    },
    {
        text: "Imperpol 4000 Negro",
        value: "Imperpol 4000 Negro"
    },
    {
        text: "Imperpol 4000 Tabaco",
        value: "Imperpol 4000 Tabaco"
    },
    {
        text: "Asfalum Rollo",
        value: "Asfalum Rollo"
    },
    {
        text: "Alumband Rollo",
        value: "Alumband Rollo"
    },
    {
        text: "Imptek parking",
        value: "Imptek parking"
    },
    {
        text: "Imptek Dren Jardín Verde Rollo",
        value: "Imptek Dren Jardín Verde Rollo"
    },
    {
        text: "Alumband 10 cm x 5 m",
        value: "Alumband 10 cm x 5 m"
    },
    {
        text: "Alumband 20 cm ax 5 m",
        value: "Alumband 20 cm ax 5 m"
    },
    {
        text: "Alumband 50 cm x 5 m",
        value: "Alumband 50 cm x 5 m"
    },
    {
        text: "Alumband 10 cm x 10 m",
        value: "Alumband 10 cm x 10 m"
    },
    {
        text: "Alumband 20 cm x 10 m",
        value: "Alumband 20 cm x 10 m"
    },
    {
        text: "Alumband 50 cm x 10 m",
        value: "Alumband 50 cm x 10 m"
    },
    {
        text: "Alumband 10 cm x 1 m",
        value: "Alumband 10 cm x 1 m"
    },
    {
        text: "Polibrea L-10",
        value: "Polibrea L-10"
    },
    {
        text: "Polibrea 20 kg",
        value: "Polibrea 20 kg"
    },
    {
        text: "Imperlastic 1 galón",
        value: "Imperlastic 1 galón"
    },
    {
        text: "Imperlastic 5 galones",
        value: "Imperlastic 5 galones"
    },
    {
        text: "Cemento Asfaltico 200 ml",
        value: "Cemento Asfaltico 200 ml"
    },
    {
        text: "Cemento Asfaltico 1/8 galon",
        value: "Cemento Asfaltico 1/8 galon"
    },
    {
        text: "Cemento Asfaltico 1/4 galón",
        value: "Cemento Asfaltico 1/4 galón"
    },
    {
        text: "Cemento Asfaltico 1 galón",
        value: "Cemento Asfaltico 1 galón"
    },
    {
        text: "Cemento Asfaltico 5 galones",
        value: "Cemento Asfaltico 5 galones"
    },
    {
        text: "DENSOBREA 4 KG",
        value: "DENSOBREA 4 KG"
    },
    {
        text: "DENSOBREA 20 KG",
        value: "DENSOBREA 20 KG"
    },
    {
        text: "DENSOBREA 200 KG",
        value: "DENSOBREA 200 KG"
    },
    {
        text: "Imperpol autoadhesivo verde",
        value: "Imperpol autoadhesivo verde"
    },
    {
        text: "Imperpol autoadhesivo rojo",
        value: "Imperpol autoadhesivo rojo"
    },
    {
        text: "Super K Autoadhesivo",
        value: "Super K Autoadhesivo"
    },
    {
        text: "POLYVENT",
        value: "POLYVENT"
    },
    {
        text: "SUPERACRYL 5 BLANCO 25 KG",
        value: "SUPERACRYL 5 BLANCO 25 KG"
    },
    {
        text: "SUPERACRYL 5 BLANCO 5 KG",
        value: "SUPERACRYL 5 BLANCO 5 KG"
    },
    {
        text: "SUPERACRYL 5 GRIS 25 KG",
        value: "SUPERACRYL 5 GRIS 25 KG"
    },
    {
        text: "SUPERACRYL 5 GRIS 5 KG",
        value: "SUPERACRYL 5 GRIS 5 KG"
    },
    {
        text: "SUPERACRYL 3 BLANCO 25 KG",
        value: "SUPERACRYL 3 BLANCO 25 KG"
    },
    {
        text: "SUPERACRYL 3 BLANCO 5 KG",
        value: "SUPERACRYL 3 BLANCO 5 KG"
    },
    {
        text: "SUPERACRYL 3 GRIS 25 KG",
        value: "SUPERACRYL 3 GRIS 25 KG"
    },
    {
        text: "SUPERACRYL 3 GRIS 5 KG",
        value: "SUPERACRYL 3 GRIS 5 KG"
    },
    {
        text: "Superacryl 10A Blanco 5 kg",
        value: "Superacryl 10A Blanco 5 kg"
    },
    {
        text: "Superacryl 10A Gris 5 kg",
        value: "Superacryl 10A Gris 5 kg"
    },
    {
        text: "Superacryl 10A Blanco 25 kg",
        value: "Superacryl 10A Blanco 25 kg"
    },
    {
        text: "Superacryl 10A Blanco 210 kg",
        value: "Superacryl 10A Blanco 210 kg"
    },
    {
        text: "Superacryl 10A Gris 210 kg",
        value: "Superacryl 10A Gris 210 kg"
    },
    {
        text: "Superacryl 10A Gris 25 kg",
        value: "Superacryl 10A Gris 25 kg"
    },
    {
        text: "SUPERACRYL IMPRIMANTE 20 KG",
        value: "SUPERACRYL IMPRIMANTE 20 KG"
    },
    {
        text: "SUPERACRYL IMPRIMANTE 4 KG",
        value: "SUPERACRYL IMPRIMANTE 4 KG"
    },
    {
        text: "SUPER MALLA",
        value: "SUPER MALLA"
    },
    {
        text: "Supermalla (Refuerzo) 22 cm  x 100 m",
        value: "Supermalla (Refuerzo) 22 cm  x 100 m"
    },
    {
        text: "Supermalla (Refuerzo) 55cm  x 100 m",
        value: "Supermalla (Refuerzo) 55cm  x 100 m"
    },
    {
        text: "Teja Ranchera Verde",
        value: "Teja Ranchera Verde"
    },
    {
        text: "Teja Ranchera Negro",
        value: "Teja Ranchera Negro"
    },
    {
        text: "Teja Ranchera Tabaco",
        value: "Teja Ranchera Tabaco"
    },
    {
        text: "Teja Renchera Roja",
        value: "Teja Renchera Roja"
    },
    {
        text: "Teja Hexagonal Verde",
        value: "Teja Hexagonal Verde"
    },
    {
        text: "Teja Hexagonal Negra",
        value: "Teja Hexagonal Negra"
    },
    {
        text: "Teja Hexagonal Tabaco",
        value: "Teja Hexagonal Tabaco"
    },
    {
        text: "Teja Hexagonal Roja",
        value: "Teja Hexagonal Roja"
    },
    {
        text: "Teja Oval Verde",
        value: "Teja Oval Verde"
    },
    {
        text: "Teja Oval Negra",
        value: "Teja Oval Negra"
    },
    {
        text: "Teja Oval Tabaco",
        value: "Teja Oval Tabaco"
    },
    {
        text: "Teja Oval Roja",
        value: "Teja Oval Roja"
    },
    {
        text: "Teja Estandar Verde",
        value: "Teja Estandar Verde"
    },
    {
        text: "Teja Estandar Negra",
        value: "Teja Estandar Negra"
    },
    {
        text: "Teja Estandar Tabaco",
        value: "Teja Estandar Tabaco"
    },
    {
        text: "Teja Estandar Roja",
        value: "Teja Estandar Roja"
    },
    {
        text: "Teja XT25 Moire Black ( Caja.3.0096)",
        value: "Teja XT25 Moire Black ( Caja.3.0096)"
    },
    {
        text: "Teja XT25 Cedar Brown ( Caja.3.0096)",
        value: "Teja XT25 Cedar Brown ( Caja.3.0096)"
    },
    {
        text: "Teja XT25 Tile Red Blend ( Caja.3.0096)",
        value: "Teja XT25 Tile Red Blend ( Caja.3.0096)"
    },
    {
        text: "TEJA XT25 CINNAMON FROST ( CAJA.3.0096)",
        value: "TEJA XT25 CINNAMON FROST ( CAJA.3.0096)"
    },
    {
        text: "TEJA XT25 SLATE GRAY ( Caja.3.0096)",
        value: "TEJA XT25 SLATE GRAY ( Caja.3.0096)"
    },
    {
        text: "TEJA XT25 TIMBER BLEND ( CAJA.3.0096)",
        value: "TEJA XT25 TIMBER BLEND ( CAJA.3.0096)"
    },
    {
        text: "Teja Xt25 Grey Frozt ( Caja.3.096)",
        value: "Teja Xt25 Grey Frozt ( Caja.3.096)"
    },
    {
        text: "Teja Land Atlantic Blue ( Caja.3.0096)",
        value: "Teja Land Atlantic Blue ( Caja.3.0096)"
    },
    {
        text: "Teja Land Hunter Green ( Caja.3.0096)",
        value: "Teja Land Hunter Green ( Caja.3.0096)"
    },
    {
        text: "Teja Land Resawn Shake ( Caja.3.0096)",
        value: "Teja Land Resawn Shake ( Caja.3.0096)"
    },
    {
        text: "Teja Land Cottage Red ( Caja.3.0096)",
        value: "Teja Land Cottage Red ( Caja.3.0096)"
    },
    {
        text: "Teja Land Burtn Sienna (caja 3.0096)",
        value: "Teja Land Burtn Sienna (caja 3.0096)"
    },
    {
        text: "Teja Land Charcoal Black (Caja 3.0096)",
        value: "Teja Land Charcoal Black (Caja 3.0096)"
    },
    {
        text: "Canal Aluminio Blanco 3 m",
        value: "Canal Aluminio Blanco 3 m"
    },
    {
        text: "Canal Aluminio Blanco 4 m",
        value: "Canal Aluminio Blanco 4 m"
    },
    {
        text: "Bajante Aluminio Blanco 3 m",
        value: "Bajante Aluminio Blanco 3 m"
    },
    {
        text: "Bajante Aluminio Blanco 4 m",
        value: "Bajante Aluminio Blanco 4 m"
    },
    {
        text: "Codo Aluminio Blanco",
        value: "Codo Aluminio Blanco"
    },
    {
        text: "Sujetador de Bajante Aluminio Blanco",
        value: "Sujetador de Bajante Aluminio Blanco"
    },
    {
        text: "Tapa derecha blanca",
        value: "Tapa derecha blanca"
    },
    {
        text: "Tapa izquierda blanca",
        value: "Tapa izquierda blanca"
    },
    {
        text: "Canal Galvalume 3 m",
        value: "Canal Galvalume 3 m"
    },
    {
        text: "Canal Galvalume 4 m",
        value: "Canal Galvalume 4 m"
    },
    {
        text: "Bajante Galvalume 3 m",
        value: "Bajante Galvalume 3 m"
    },
    {
        text: "Bajante Galvalume 4 m",
        value: "Bajante Galvalume 4 m"
    },
    {
        text: "Codo Galvalume",
        value: "Codo Galvalume"
    },
    {
        text: "Sujetador de Canal",
        value: "Sujetador de Canal"
    },
    {
        text: "Sujetador de Bajante Galvanizado",
        value: "Sujetador de Bajante Galvanizado"
    },
    {
        text: "Tapa Derecha Galvalume",
        value: "Tapa Derecha Galvalume"
    },
    {
        text: "Tapa Izquierda Galvalume",
        value: "Tapa Izquierda Galvalume"
    },
    {
        text: "Piton",
        value: "Piton"
    },
    {
        text: "AISLANTEK 2''",
        value: "AISLANTEK 2'' "
    },
    {
        text: "Aislantek 1''",
        value: "Aislantek 1''"
    },
    {
        text: "Bitumix 25 Kg",
        value: "Bitumix 25 Kg"
    },
    {
        text: "Emulsion Cationica Css - 1h",
        value: "Emulsion Cationica Css - 1h"
    },
    {
        text: "Mezcla Asfaltica Ecológica",
        value: "Mezcla Asfaltica Ecológica"
    }
]

export default data;
