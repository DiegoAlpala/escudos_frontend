import { Component, Input, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogCloseResult, DialogRef, DialogService } from '@progress/kendo-angular-dialog';
import { switchMap } from 'rxjs/operators';
import { ImpermeabilizacionSistema } from 'src/app/_dominio/administracion/impermeabilizacionSistema';
import { Material } from 'src/app/_dominio/administracion/material';
import { MaterialCatalogo } from 'src/app/_dominio/administracion/material-catalogo';
import { Obra } from 'src/app/_dominio/administracion/obra';
import { MaterialCatalogoService } from 'src/app/_servicio/adminitracion/material-catalogo.service';
import { MaterialService } from 'src/app/_servicio/adminitracion/material.service';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { SistemasService } from 'src/app/_servicio/adminitracion/sistemas.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';

const createFormGroup = dataItem => new FormGroup({
    'id': new FormControl(dataItem.id),
    'tipo': new FormControl(dataItem.tipo, Validators.required),
    'nombre': new FormControl(dataItem.nombre, Validators.required),
    'tipoObra': new FormControl(dataItem.tipoObra),
    'dimension': new FormControl(dataItem.dimension)
});

const createFormGroupMaterial = dataItem => new FormGroup({
    'id': new FormControl(dataItem.id),
    'nombreProducto': new FormControl(dataItem.nombreProducto, Validators.required),
    'lote': new FormControl(dataItem.lote),
    'cantidad': new FormControl(dataItem.cantidad),
    'unidad': new FormControl(dataItem.unidad),
    'materialCatalogo': new FormControl(dataItem.materialCatalogo)
});

@Component({
    selector: 'app-obras-editar-sistema',
    templateUrl: './obras-editar-sistema.component.html',
    styleUrls: ['./obras-editar-sistema.component.scss']
})
export class ObrasEditarSistemaComponent implements OnInit {

    @Input()
    obra: Obra;

    public gridData: ImpermeabilizacionSistema[] = [];
    private editedRowIndex: number;

    public sistemaSeleccionado: ImpermeabilizacionSistema;
    public formGroup: FormGroup;

    /* Materiales */
    public unidadesCatalogo = ['Rollo', 'Caneca', 'Galón', 'Caja', 'Unidad', 'PQ5', 'Saco'];
    public productos: MaterialCatalogo[] = [];
    public gridDataMaterial: Material[] = [];
    public formGroupMaterial: FormGroup;
    private editedRowIndexMaterial: number;
    public editarDetalle: boolean = false;

    @ViewChild("containerConfirmacion", { read: ViewContainerRef })
    public containerConfirmacionRef: ViewContainerRef;

    constructor(
        private _notificarService: NotificarService,
        private _obraService: ObraService,
        private _sistemasService: SistemasService,
        private _materialService: MaterialService,
        private dialogService: DialogService,
        private _materialCatalogoService: MaterialCatalogoService,
    ) { }

    ngOnInit(): void {
        this.gridData = this.obra.sistemas;
        this.obtenerCatalogos();
    }

    private obtenerCatalogos() {
        this._materialCatalogoService.listarActivos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this.productos = data;
        })
    }

    public addHandler({ sender }) {
        this.closeEditor(sender);
        this.formGroup = createFormGroup({
            'id': 0,
            'tipo': '',
            'nombre': '',
            'tipoObra': '',
            'dimension': 0
        });
        sender.addRow(this.formGroup);
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);
        this.formGroup = createFormGroup(dataItem);
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, formGroup, isNew }): void {
        const sistema = formGroup.value;
        this.guardar(sistema);
        sender.closeRow(rowIndex);
    }

    public removeHandler({ dataItem }): void {
        //this.service.remove(dataItem);
        this.mostrarConfirmacion(dataItem, 'sistema');
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

    private guardar(sistema) {
        if (sistema.id === 0) {
            this._obraService.agregarSistema(sistema, this.obra.id).pipe(switchMap(() => {
                return this._obraService.listarPorId(this.obra.id);
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Sistema agregado");
                this._obraService.obraActualizado.next(data);
                this.gridData = data.sistemas;
            });

        } else {
            this._sistemasService.modificar(sistema).pipe(switchMap(() => {
                return this._obraService.listarPorId(this.obra.id);
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Sistema actualizado");
                this._obraService.obraActualizado.next(data);
                this.gridData = data.sistemas;
            });
        }
    }

    public verDetalle(sistema: ImpermeabilizacionSistema) {
        this.sistemaSeleccionado = sistema;
        this.gridDataMaterial = sistema.materiales;
        this.editarDetalle = true;
    }

    public addHandlerM({ sender }) {
        this.closeEditor(sender);
        this.formGroupMaterial = createFormGroupMaterial({
            'id': 0,
            'nombreProducto': '',
            'lote': '',
            'cantidad': 0,
            'unidad': '',
            'materialCatalogo': null
        });
        sender.addRow(this.formGroupMaterial);
    }

    public editHandlerM({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);
        this.formGroupMaterial = createFormGroupMaterial(dataItem);
        this.editedRowIndexMaterial = rowIndex;
        sender.editRow(rowIndex, this.formGroupMaterial);
    }

    public cancelHandlerM({ sender, rowIndex }) {
        this.closeEditorM(sender, rowIndex);
    }

    public saveHandlerM({ sender, rowIndex, formGroup, isNew }): void {
        const material = this.formGroupMaterial.value;
        this.guardarMaterial(material);
        sender.closeRow(rowIndex);
    }

    public removeHandlerM({ dataItem }): void {
        //this.service.remove(dataItem);
        this.mostrarConfirmacion(dataItem, 'material');
    }

    private closeEditorM(grid, rowIndex = this.editedRowIndexMaterial) {
        grid.closeRow(rowIndex);
        this.editedRowIndexMaterial = undefined;
        this.formGroupMaterial = undefined;
    }

    private guardarMaterial(material) {
        if (material.id === 0) {
            this._sistemasService.agregarMaterial(material, this.sistemaSeleccionado.id).pipe(switchMap(() => {
                return this._obraService.listarPorId(this.obra.id);
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Material agregado");
                this._obraService.obraActualizado.next(data);
                this.gridDataMaterial = data.sistemas.find(x => x.id === this.sistemaSeleccionado.id).materiales;
            });

        } else {
            this._materialService.modificar(material).pipe(switchMap(() => {
                return this._obraService.listarPorId(this.obra.id);
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Material actualizado");
                this._obraService.obraActualizado.next(data);
                this.gridDataMaterial = data.sistemas.find(x => x.id === this.sistemaSeleccionado.id).materiales;
            });
        }
    }

    public regresar() {
        this.editarDetalle = false;
        this.sistemaSeleccionado = null;
    }

    public mostrarConfirmacion(item: any, tipo: string) {
        let mensaje: string;
        if (tipo === 'sistema')
            mensaje = `¿Está seguro de eliminar el Sistema ${item.nombre} y todos sus materiales?`;
        else
            mensaje = `¿Está seguro de eliminar el Material ${item.nombreProducto} ?`;

        const dialog: DialogRef = this.dialogService.open({
            appendTo: this.containerConfirmacionRef,
            title: 'Confirmación',
            content: mensaje,
            actions: [
                { text: 'No' },
                { text: 'Sí', primary: true }
            ],
            width: 450,
            height: 200,
            minWidth: 200
        });

        dialog.result.subscribe((result) => {
            if (result instanceof DialogCloseResult) {

            } else {
                if (result['text'] === 'Sí') {
                    if (tipo === 'sistema')
                        this.eliminarSistemas(item.id);
                    else
                        this.eliminarMateriales(item.id);
                }
            }
        });
    }

    private eliminarSistemas(id: number) {
        this._sistemasService.eliminarSistema(id).pipe(switchMap(() => {
            return this._obraService.listarPorId(this.obra.id);
        })).subscribe(data => {
            this._notificarService.desactivarLoading();
            this._notificarService.mostrarMensajeExito("Sistema eliminado");
            this._obraService.obraActualizado.next(data);
            this.gridData = data.sistemas;
        });
    }

    private eliminarMateriales(id: number) {
        this._materialService.eliminarMaterial(id).pipe(switchMap(() => {
            return this._obraService.listarPorId(this.obra.id);
        })).subscribe(data => {
            this._notificarService.desactivarLoading();
            this._notificarService.mostrarMensajeExito("Sistema eliminado");
            this._obraService.obraActualizado.next(data);
            this.gridDataMaterial = data.sistemas.find(x => x.id === this.sistemaSeleccionado.id).materiales;
        });
    }

}
