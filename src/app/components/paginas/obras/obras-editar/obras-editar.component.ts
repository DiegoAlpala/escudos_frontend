import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { Obra } from 'src/app/_dominio/administracion/obra';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { FechaService } from 'src/app/_servicio/fecha-service';
import { NotificarService } from 'src/app/_servicio/notificar.service';

@Component({
    selector: 'app-obras-editar',
    templateUrl: './obras-editar.component.html',
    styleUrls: ['./obras-editar.component.scss']
})
export class ObrasEditarComponent implements OnInit {

    @Input()
    nombreComponentePadre: String;

    public id: number;
    public obraEdicion: Obra;

    public regionesLista: any[] = [];

    // google maps zoom level
    zoom: number = 16;

    // initial center position for the map
    lat: number = 51.673858;
    lng: number = 7.815982;

    public formObra = new FormGroup({
        'id': new FormControl(0),
        'serie': new FormControl(null),
        'anios': new FormControl(null),
        'region': new FormControl('', Validators.required),
        'fechaInicioGarantia': new FormControl(),
        'longitud': new FormControl(0),
        'latitud': new FormControl(0),
        'dimension': new FormControl(),
        'nombrePropietario': new FormControl(''),
        'razonSocial': new FormControl(''),
        'socio': new FormControl(''),
        'ubicacionEscudo': new FormControl(''),
        'ciudad': new FormControl(''),
        'comentario': new FormControl(''),
    });

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _notificarService: NotificarService,
        private _obraService: ObraService,
        private _fechaService: FechaService
    ) { }

    ngOnInit(): void {
        this._obraService.obraActualizado.subscribe(data => {
            this.obraEdicion = data;
        });

        this.route.params.subscribe((params: Params) => {
            this.id = params['id'];
            this.setearListas().subscribe(data => {
                this._notificarService.loadingCambio.next(false);
                this.regionesLista = data[0];
                this.recuperarObra();
            })
        })
    }

    getLocation() {
        this._obraService.getPosition().then(pos => {
            if(this.obraEdicion.longitud){
                this.lat = this.obraEdicion.latitud;
                this.lng = this.obraEdicion.longitud;
            }else{
                this.lat = pos.lat;
                this.lng = pos.lng;
            }

        });
    }

    private setearListas(): Observable<any[]> {
        let region = this._obraService.obtenerCatalogoRegiones();
        return forkJoin([region]);
    }

    private recuperarObra() {
        if (this.id)
            this._obraService.listarPorId(this.id).subscribe(data => {
                this._notificarService.desactivarLoading();
                this.obraEdicion = data;
                this.inicializarFormulario();
            });
    }

    private inicializarFormulario() {
        if (this.obraEdicion) {
            this.formObra.controls['id'].setValue(this.obraEdicion.id);
            this.formObra.controls['anios'].setValue(this.obraEdicion.anios);
            this.formObra.controls['serie'].setValue(this.obraEdicion.serie);
            this.formObra.controls['region'].setValue(this.obraEdicion.region);
            if (this.obraEdicion && this.obraEdicion.fechaInicioGarantia) {
                const a = new Date(this.obraEdicion.fechaInicioGarantia.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                this.formObra.controls['fechaInicioGarantia'].setValue(a);
            }
            this.formObra.controls['longitud'].setValue(this.obraEdicion.longitud);
            this.formObra.controls['latitud'].setValue(this.obraEdicion.latitud);
            this.formObra.controls['dimension'].setValue(this.obraEdicion.dimension);
            this.formObra.controls['nombrePropietario'].setValue(this.obraEdicion.nombrePropietario);
            this.formObra.controls['razonSocial'].setValue(this.obraEdicion.razonSocial);
            this.formObra.controls['socio'].setValue(this.obraEdicion.socio.nombre);
            this.formObra.controls['ubicacionEscudo'].setValue(this.obraEdicion.ubicacionEscudo);
            this.formObra.controls['ciudad'].setValue(this.obraEdicion.ciudad);
            this.formObra.controls['comentario'].setValue(this.obraEdicion.comentario);
            this.getLocation();
        }
    }

    public guardar() {
        const obraActualizar = this.crearObraObj();
        this._obraService.modificar(obraActualizar).subscribe(data => {
            this._notificarService.desactivarLoading();
            this._notificarService.mostrarMensajeExito('Datos actualizados.');
        });
    }

    private crearObraObj() {
        const obraActualizar = new Obra();
        obraActualizar.id = this.id;
        obraActualizar.latitud = this.formObra.controls['latitud'].value;
        obraActualizar.serie = this.formObra.controls['serie'].value;
        obraActualizar.longitud = this.formObra.controls['longitud'].value;
        obraActualizar.anios = this.formObra.controls['anios'].value;
        obraActualizar.nombrePropietario = this.formObra.controls['nombrePropietario'].value;
        if (this.formObra.controls['fechaInicioGarantia'].value)
            obraActualizar.fechaInicioGarantia = this._fechaService.formatearFecha(this.formObra.controls['fechaInicioGarantia'].value);
        obraActualizar.dimension = this.formObra.controls['dimension'].value;
        obraActualizar.region = this.formObra.controls['region'].value;
        obraActualizar.razonSocial = this.formObra.controls['razonSocial'].value;
        obraActualizar.ubicacionEscudo = this.formObra.controls['ubicacionEscudo'].value;
        obraActualizar.ciudad = this.formObra.controls['ciudad'].value;
        obraActualizar.comentario = this.formObra.controls['comentario'].value;
        return obraActualizar;
    }

    public cancelar() {
        this._obraService.listarTodos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this._obraService.obrasCambio.next(data);
            this.router.navigate(['obras']);
        })
    }

    mapClicked(event: any) {
        console.log(event)
        const lat = event.coords.lat;
        const lng = event.coords.lng;
        this.formObra.controls['latitud'].setValue(lat);
        this.formObra.controls['longitud'].setValue(lng);

    }

    markerDragEnd(event: any) {
        const lat = event.coords.lat;
        const lng = event.coords.lng;
        this.formObra.controls['latitud'].setValue(lat);
        this.formObra.controls['longitud'].setValue(lng);
    }

    obtenerLatitud() {
        return this.formObra.controls['latitud'].value;
    }

    obtenerLongitud() {
        return this.formObra.controls['longitud'].value;
    }

}
