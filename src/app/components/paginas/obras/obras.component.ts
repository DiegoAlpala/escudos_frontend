import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';
import { Obra } from 'src/app/_dominio/administracion/obra';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';

@Component({
    selector: 'app-obras',
    templateUrl: './obras.component.html',
    styleUrls: ['./obras.component.scss']
})
export class ObrasComponent implements OnInit {

    public obrasLista: Obra[] = [];
    public gridObrasView: GridDataResult;

    public state: State = {
        skip: 0,
        take: 10,
        sort: [],
        filter: {
            logic: 'and',
            filters: [{ field: 'nombre', operator: 'contains', value: '' }]
        }
    }

    constructor(
        private _obraService: ObraService,
        private _notificarService: NotificarService,
        public route : ActivatedRoute
    ) { }

    ngOnInit(): void {
        this._obraService.obrasCambio.subscribe(data => {
            this.refrescarGrid(data);
        })
        this.listarSocios();
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridObrasView = process(this.obrasLista, this.state);
    }

    private listarSocios() {
        this._obraService.listarTodos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this.refrescarGrid(data);
        })
    }

    private refrescarGrid(data: Obra[]) {
        this.obrasLista = data;
        this.gridObrasView = process(data, this.state);
    }

    public descargarQR(item: Obra){
        this._obraService.generarQR(item.id).subscribe(data =>{
            this._notificarService.desactivarLoading();
            const file = new Blob([data], { type: 'image/png' });
            const fileURL = URL.createObjectURL(file);
            const a = document.createElement('a');
            a.href = fileURL;
            a.download = `${item.serie}`;
            a.click();
        })
    }

    public descargarEscudoQR(item: Obra){
        this._obraService.generarReporteEscudoQr(item.id).subscribe(data =>{
            this._notificarService.desactivarLoading();
            const file = new Blob([data], { type: 'pdf' });
            const fileURL = URL.createObjectURL(file);
            const a = document.createElement('a');
            a.href = fileURL;
            a.download = `${item.serie}.pdf`;
            a.click();
        })
    }

}
