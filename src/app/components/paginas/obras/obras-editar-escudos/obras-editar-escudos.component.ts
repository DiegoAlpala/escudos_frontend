import { Component, Input, OnInit } from '@angular/core';
import { EscudoGarantia } from 'src/app/_dominio/administracion/escudoGarantia';
import { Obra } from 'src/app/_dominio/administracion/obra';

@Component({
    selector: 'app-obras-editar-escudos',
    templateUrl: './obras-editar-escudos.component.html',
    styleUrls: ['./obras-editar-escudos.component.scss']
})
export class ObrasEditarEscudosComponent implements OnInit {

    @Input()
    obra: Obra;
    public escudos: EscudoGarantia[] = [];


    public escudoOro = "assets/img/oro.svg";
    public escudoPlata = "assets/img/plata.svg";
    public escudoBronce = "assets/img/bronce.svg";
    public escudoPlatinium = "assets/img/platinium.svg";

    constructor() { }

    ngOnInit(): void {
        this.escudos = this.obra.escudos;
    }

    public determinarEscudo(tipoGarantia: string) {
        switch (tipoGarantia) {
            case 'ORO':
                return this.escudoOro;
            case 'PLATA':
                return this.escudoPlata;
            case 'BRONCE':
                return this.escudoBronce;
            case 'PLATINO':
                return this.escudoPlatinium;
            default:
                break;
        }

    }

}
