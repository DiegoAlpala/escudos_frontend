import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Configuracion } from 'src/app/_dominio/sistema/configuracion';
import { NotificarService } from 'src/app/_servicio/notificar.service';
import { ConfiguracionService } from 'src/app/_servicio/sistema/configuracion.service';

const createFormGroup = dataItem => new FormGroup({
    'id': new FormControl(dataItem.id),
    'nombreConfiguracion': new FormControl(dataItem.nombreConfiguracion, Validators.required),
    'descripcion': new FormControl(dataItem.descripcion, Validators.required),
    'valorConfiguracion': new FormControl(dataItem.valorConfiguracion)
});

@Component({
    selector: 'app-configuracion',
    templateUrl: './configuracion.component.html',
    styleUrls: ['./configuracion.component.scss']
})
export class ConfiguracionComponent implements OnInit {

    public formGroup: FormGroup;
    public gridData: Configuracion[] = [];
    private editedRowIndex: number;

    constructor(
        private _configuracionService: ConfiguracionService,
        private _notificarService: NotificarService
    ) { }

    ngOnInit(): void {
        this.listar();
    }

    private listar() {
        this._configuracionService.listarTodos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this.gridData = data;
        })
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);
        this.formGroup = createFormGroup(dataItem);
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, formGroup, isNew }): void {
        const sistema = formGroup.value;
        this.guardar(sistema);
        sender.closeRow(rowIndex);
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

    private guardar(sistema) {
        this._configuracionService.modificar(sistema).pipe(switchMap(() => {
            return this._configuracionService.listarTodos();
        })).subscribe(data => {
            this._notificarService.desactivarLoading();
            this._notificarService.mostrarMensajeExito("Configuración actualizada");
            this._configuracionService.menuCambio.next(data);
            this.gridData = data;
        });
    }
}
