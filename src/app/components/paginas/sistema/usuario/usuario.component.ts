import { switchMap } from 'rxjs/operators';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { DialogService } from '@progress/kendo-angular-dialog';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';
import { Usuario } from 'src/app/_dominio/sistema/usuario';
import { NotificarService } from 'src/app/_servicio/notificar.service';
import { UsuarioService } from 'src/app/_servicio/sistema/usuario.service';
import { UsuarioRegistrarComponent } from './usuario-registrar/usuario-registrar.component';
import { Rol } from 'src/app/_dominio/sistema/rol';
import { UsuarioResetPasswordComponent } from './usuario-reset-password/usuario-reset-password.component';

@Component({
    selector: 'app-usuario',
    templateUrl: './usuario.component.html',
    styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

    public usuariosLista: Usuario[] = [];
    public usuarioNuevo: boolean = true;
    public usuarioEditar: Usuario;

    public gridUsuariosView: GridDataResult;

    public state: State = {
        skip: 0,
        take: 10,
        sort: [],
        filter: {
            logic: 'and',
            filters: [{ field: 'nombre', operator: 'contains', value: '' }]
        }
    }

    @ViewChild("containerRegistro", { read: ViewContainerRef })
    public containerRegsitroRef: ViewContainerRef;
    @ViewChild("containerContrasena", { read: ViewContainerRef })
    public containerContrasenaRef: ViewContainerRef;

    constructor(
        private _notificarService: NotificarService,
        private _usuarioService: UsuarioService,
        private dialogService: DialogService,
    ) { }

    ngOnInit(): void {
        this._usuarioService.usuariosCambio.subscribe(data => {
            this.refrescarGrid(data);
        })
        this.listarUsuarios();
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridUsuariosView = process(this.usuariosLista, this.state);
    }

    private listarUsuarios() {
        this._usuarioService.listarTodos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this.refrescarGrid(data);
        })
    }

    private refrescarGrid(data: Usuario[]) {
        this.usuariosLista = data;
        this.gridUsuariosView = process(data, this.state);
    }

    public abrirRegistroDialogo() {
        const dialogRefRegistro = this.dialogService.open({
            appendTo: this.containerRegsitroRef,
            content: UsuarioRegistrarComponent,
            minWidth: 400,
            maxWidth: 500,
            maxHeight: 700,
            title: 'REGISTRO',
            actions: [{ text: 'Cancelar' }, { text: 'Aceptar', primary: true }],
            preventAction: (ev, dialog) => {
                if (ev['text'] === 'Aceptar')
                    return !dialog.content.instance.editForm.valid;
                else
                    return false;
            }
        });

        const usuarioRegistro = dialogRefRegistro.content.instance;
        usuarioRegistro.usuarioEditar = this.usuarioEditar
        dialogRefRegistro.result.subscribe(r => {
            if (r['text'] == 'Aceptar') {
                this.guardarUsuario(usuarioRegistro.editForm.value);
                dialogRefRegistro.close();
            }
            if (r['text'] == 'Cancelar') {
                dialogRefRegistro.close();
            }
        });
    }

    public editarUsuario(item: Usuario) {
        this.usuarioEditar = item;
        this.abrirRegistroDialogo();
    }

    public crearUsuario() {
        this.usuarioEditar = null;
        this.abrirRegistroDialogo();
    }

    private guardarUsuario(formulario) {
        const usuario = this.crearUsuarioObj(formulario);
        if (!this.usuarioEditar) {
            this._usuarioService.registrar(usuario).pipe(switchMap(() => {
                return this._usuarioService.listarTodos();
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Usuario Regsitrado");
                this.refrescarGrid(data);
            })
        } else {
            this._usuarioService.modificar(usuario).pipe(switchMap(() => {
                return this._usuarioService.listarTodos();
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Usuario Modificado");
                this.refrescarGrid(data);
            })
        }
    }

    private crearUsuarioObj(formulario) {
        const usuario = new Usuario();
        const rol = new Rol();
        usuario.id = formulario.id;
        usuario.nombreUsuario = formulario.nombreUsuario;
        usuario.nombreCompleto = formulario.nombreCompleto;
        usuario.correo = formulario.correo;
        usuario.tipo = formulario.tipo;
        rol.id = formulario.rol;
        usuario.rol = rol;
        usuario.activo = formulario.activo;
        usuario.contrasena = formulario.contrasena;
        return usuario;
    }

    public cambiarContrasena(item: Usuario) {
        this.abrirCambiarContrasenaDialogo(item);
    }

    public abrirCambiarContrasenaDialogo(usuario: Usuario) {
        const dialogRefContrasena = this.dialogService.open({
            appendTo: this.containerContrasenaRef,
            content: UsuarioResetPasswordComponent,
            minWidth: 600,
            maxWidth: 600,
            maxHeight: 400,
            title: `CAMBIO CONTRASEÑA -> ${usuario.nombreCompleto}`,
            actions: [{ text: 'Cancelar' }, { text: 'Aceptar', primary: true }],
            preventAction: (ev, dialog) => {
                if (ev['text'] === 'Aceptar')
                    return !dialog.content.instance.registerForm.valid;
                else
                    return false;
            }
        });

        const contrasenaRegistro = dialogRefContrasena.content.instance;
        dialogRefContrasena.result.subscribe(r => {
            if (r['text'] == 'Aceptar') {
                this.resetContrasena(usuario.id, contrasenaRegistro.registerForm.value.password);
                dialogRefContrasena.close();
            }
            if (r['text'] == 'Cancelar') {
                dialogRefContrasena.close();
            }
        });
    }

    private resetContrasena(usuarioId, contrasena) {
        this._usuarioService.cambiarContrasena(usuarioId, contrasena).subscribe(data => {
            this._notificarService.desactivarLoading();
            this._notificarService.mostrarMensajeExito(`Contraseña cambiada exitosamente.`);
        });
    }

}
