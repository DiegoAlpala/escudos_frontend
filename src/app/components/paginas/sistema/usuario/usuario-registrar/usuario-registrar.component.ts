import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Observable } from 'rxjs';
import { Rol } from 'src/app/_dominio/sistema/rol';
import { Usuario } from 'src/app/_dominio/sistema/usuario';
import { NotificarService } from 'src/app/_servicio/notificar.service';
import { MenuService } from 'src/app/_servicio/sistema/menu.service';
import { UsuarioService } from 'src/app/_servicio/sistema/usuario.service';

@Component({
    selector: 'app-usuario-registrar',
    templateUrl: './usuario-registrar.component.html',
    styleUrls: ['./usuario-registrar.component.scss']
})
export class UsuarioRegistrarComponent implements OnInit {

    public usuarioEditar: Usuario;
    public tiposCatalogo: any[] = [];
    public rolesCatalogo: Rol[] = [];

    public editForm: FormGroup = new FormGroup({
        id: new FormControl(0),
        nombreUsuario: new FormControl('', Validators.required),
        nombreCompleto: new FormControl('', [Validators.required]),
        correo: new FormControl('', Validators.required),
        activo: new FormControl(1),
        rol: new FormControl('', Validators.required),
        tipo: new FormControl('', Validators.required),
        contrasena: new FormControl('', Validators.required),
        contrasenaConfirm: new FormControl('', Validators.required),
    });

    constructor(
        private _notificarService: NotificarService,
        private _usuarioService: UsuarioService,
        private _menuService: MenuService
    ) { }

    ngOnInit(): void {

        this.setearListas().subscribe(data => {
            this._notificarService.loadingCambio.next(false);
            this.tiposCatalogo = data[0];
            this.rolesCatalogo = data[1];
            this.cargarDatos();
        });

    }

    private cargarDatos() {
        if (this.usuarioEditar) {
            this.editForm.controls['id'].setValue(this.usuarioEditar.id);
            this.editForm.controls['nombreUsuario'].setValue(this.usuarioEditar.nombreUsuario);
            this.editForm.controls['nombreCompleto'].setValue(this.usuarioEditar.nombreCompleto);
            this.editForm.controls['correo'].setValue(this.usuarioEditar.correo);
            this.editForm.controls['activo'].setValue(this.usuarioEditar.activo);
            this.editForm.controls['rol'].setValue(this.usuarioEditar.rol.id);
            this.editForm.controls['tipo'].setValue(this.usuarioEditar.tipo);
            this.editForm.controls['contrasena'].setValue(this.usuarioEditar.contrasena);

            this.editForm.controls['contrasenaConfirm'].clearValidators();
            this.editForm.controls['contrasenaConfirm'].updateValueAndValidity();
        }
    }

    public esEdicion() {
        return this.usuarioEditar ? true : false;
    }

    private setearListas(): Observable<any[]> {
        let tipos = this._usuarioService.listarTipoUsuario();
        let roles = this._menuService.listarTodosRoles();
        return forkJoin([tipos, roles]);
    }

    public changeValueTipo() {

    }

}
