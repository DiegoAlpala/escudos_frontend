import { Component, OnInit } from '@angular/core';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { forkJoin, Observable } from 'rxjs';
import { SocioTecnico } from 'src/app/_dominio/administracion/socioTecnico';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { SocioTecnicoService } from 'src/app/_servicio/adminitracion/socio-tecnico.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';

@Component({
    selector: 'app-consultas',
    templateUrl: './consultas.component.html',
    styleUrls: ['./consultas.component.scss']
})
export class ConsultasComponent implements OnInit {

    public listaSocios: SocioTecnico[] = [];
    public listaEstado: any[] = [];
    public listaTipoGarantia: any[] = [];

    public consulta: any = {
        'idSocio': null,
        'estado': null,
        'tipoGarantia': null,
        'serie': null
    };

    public gridView: GridDataResult;
    public pageSize = 20;
    public skip = 0;
    private currentPage: number = 0;

    constructor(
        private _notificarService: NotificarService,
        private _obraService: ObraService,
        private _socioService: SocioTecnicoService
    ) { }

    ngOnInit(): void {
        this.cargarDatos();
    }


    private cargarDatos() {
        let estadosObs = this._obraService.obtenerCatalogoEstadoObra();
        let sociosObs = this._socioService.listarTodos();
        let tipoGarantiaObs = this._obraService.obtenerCatalogoTipoGarantia();

        forkJoin([estadosObs, sociosObs, tipoGarantiaObs]).subscribe(data => {
            this._notificarService.desactivarLoading();
            this.listaEstado = data[0];
            this.listaSocios = data[1];
            this.listaTipoGarantia = data[2];

        });
    }

    public pageChange(event: PageChangeEvent): void {
        console.log(`pagina ${event.skip / event.take}`)
        this.currentPage = (event.skip / event.take);

        this.skip = event.skip;

        this.obtenerDatosConsulta(this.currentPage, this.pageSize);
    }

    obtenerDatosConsulta(page: number, size: number) {
        this._obraService.consultarObras(page, size, this.consulta).subscribe(data => {
            this._notificarService.desactivarLoading();
            this.gridView = {
                data: data.content,
                total: data.totalElements
            }
        });
    }

    consultar() {
        this.consulta.fechaInicio = null;
        this.consulta.fechaFin = null;
        if (this.verificarObjetoVacio(this.consulta)) {

            this.currentPage = 0;
            this.skip = 0;
            this.obtenerDatosConsulta(this.currentPage, this.pageSize);
        } else {
            this._notificarService.mostrarMensajeError('Ingrese al menos un criterio de búsqueda');
        }
    }

    private verificarObjetoVacio(objeto) {
        if (objeto === null) {
            return false;
        }

        for (let key in objeto) {
            if (objeto[key] !== null && objeto[key] !== '') {
                return true;
            }
        }
        return false;
    };

    public inicio() {
        this.gridView = {
            data: Array<any>(),
            total: 0
        }
    }

    public limpiar() {
        this.consulta = {
            'idSocio': null,
            'estado': null,
            'tipoGarantia': null,
            'serie': null
        };
        this.inicio();
    }

    public descargarReporte() {
        if (this.verificarObjetoVacio(this.consulta)) {

            this._obraService.generarReporteObras(this.consulta).subscribe(data => {
                this._notificarService.desactivarLoading();
                const file = new Blob([data], { type: 'application/vnd.ms-excel' });
                const fileURL = URL.createObjectURL(file);
                const a = document.createElement('a');
                a.href = fileURL;
                a.download = 'Reporte';
                a.click();
            });
        } else {
            this._notificarService.mostrarMensajeError('Ingrese al menos un criterio de búsqueda');
        }
    }

}
