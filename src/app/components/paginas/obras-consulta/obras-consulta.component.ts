import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';
import { ConsultaQR } from 'src/app/_dominio/administracion/consultaQR_DTO';
import { Obra } from 'src/app/_dominio/administracion/obra';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';

@Component({
	selector: 'app-obras-consulta',
	templateUrl: './obras-consulta.component.html',
	styleUrls: ['./obras-consulta.component.scss']
})
export class ObrasConsultaComponent implements OnInit {

	public obrasLista: ConsultaQR[] = [];
    public gridObrasView: GridDataResult;

    public state: State = {
        skip: 0,
        take: 10,
        sort: [],
        filter: {
            logic: 'and',
            filters: [{ field: 'serie', operator: 'contains', value: '' }]
        }
    }

	constructor(
		private _obraService: ObraService,
        private _notificarService: NotificarService,
	) { }

	ngOnInit(): void {
		this.listarObras();
	}

	public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridObrasView = process(this.obrasLista, this.state);
    }

    private listarObras() {
        this._obraService.consultarQRTodos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this.refrescarGrid(data);
        })
    }

    private refrescarGrid(data: ConsultaQR[]) {
        this.obrasLista = data;
        this.gridObrasView = process(data, this.state);
    }

    public descargarQR(item: Obra){
        this._obraService.generarQR(item.id).subscribe(data =>{
            this._notificarService.desactivarLoading();
            const file = new Blob([data], { type: 'image/png' });
            const fileURL = URL.createObjectURL(file);
            const a = document.createElement('a');
            a.href = fileURL;
            a.download = `${item.serie}`;
            a.click();
        })
    }

}
