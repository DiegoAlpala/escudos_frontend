import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Obra } from 'src/app/_dominio/administracion/obra';
import { ObraService } from 'src/app/_servicio/adminitracion/obra.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';
import { PublicacionesService } from 'src/app/_servicio/publico/publicaciones.service';

@Component({
    selector: 'app-vista-publica-qr',
    templateUrl: './vista-publica-qr.component.html',
    styleUrls: ['./vista-publica-qr.component.scss']
})
export class VistaPublicaQrComponent implements OnInit {

    id: string;
    public obra: Obra;

    public escudoOro = "assets/img/oro.svg";
    public escudoPlata = "assets/img/plata.svg";
    public escudoBronce = "assets/img/bronce.svg";
    public escudoPlatinium = "assets/img/platinium.svg";

    public imagenSuperior = "assets/img/fondo-superior.svg";
    public mostrarEnlace = false;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _notificarService: NotificarService,
        private _publicacionService: PublicacionesService
    ) { }

    ngOnInit(): void {
        this.route.params.subscribe((params: Params) => {
            this.id = params['serie'];
            this._publicacionService.listarPorSerie(this.id).subscribe(data => {
                this._notificarService.desactivarLoading();
                this.obra = data;
                console.log(data)
                this.mostrarEnlace = this.obra.socio.enlace !== null && this.obra.socio.enlace !== '' && this.obra.socio.enlace !== undefined ;
            });
        })
    }

    public determinarImagen() {
        if (this.obra) {
            if (this.obra.escudos.length > 0) {
                switch (this.obra.escudos[0].tipoGarantia) {
                    case 'ORO':
                        return this.escudoOro;
                    case 'PLATA':
                        return this.escudoPlata;
                    case 'BRONCE':
                        return this.escudoBronce;
                    case 'PLATINO':
                        return this.escudoPlatinium;
                    default:
                        break;
                }
            }
        }
    }

    public getTipoGarantia() {
        if (this.obra.escudos.length > 0)
            return this.obra.escudos[0].tipoGarantia

        return '';
    }
}
