import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { DialogService } from '@progress/kendo-angular-dialog';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';
import { switchMap } from 'rxjs/operators';
import { SocioTecnico } from 'src/app/_dominio/administracion/socioTecnico';
import { SocioTecnicoService } from 'src/app/_servicio/adminitracion/socio-tecnico.service';
import { NotificarService } from 'src/app/_servicio/notificar.service';
import { SocioTecnicoRegistrarComponent } from './socio-tecnico-registrar/socio-tecnico-registrar.component';

@Component({
    selector: 'app-socio-tecnico',
    templateUrl: './socio-tecnico.component.html',
    styleUrls: ['./socio-tecnico.component.scss']
})
export class SocioTecnicoComponent implements OnInit {

    public sociosLista: SocioTecnico[] = [];
    public socioNuevo: boolean = true;
    public socioEditar: SocioTecnico;

    public gridSociosView: GridDataResult;

    public state: State = {
        skip: 0,
        take: 10,
        sort: [],
        filter: {
            logic: 'and',
            filters: [{ field: 'nombre', operator: 'contains', value: '' }]
        }
    }

    @ViewChild("containerRegistro", { read: ViewContainerRef })
    public containerRegsitroRef: ViewContainerRef;

    constructor(
        private _socioTecnicoService: SocioTecnicoService,
        private _notificarService: NotificarService,
        private dialogService: DialogService,
    ) { }

    ngOnInit(): void {
        this._socioTecnicoService.socioCambio.subscribe(data => {
            this.refrescarGrid(data);
        })
        this.listarSocios();
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridSociosView = process(this.sociosLista, this.state);
    }

    private listarSocios() {
        this._socioTecnicoService.listarTodos().subscribe(data => {
            this._notificarService.desactivarLoading();
            this.refrescarGrid(data);
        })
    }

    private refrescarGrid(data: SocioTecnico[]) {
        this.sociosLista = data;
        this.gridSociosView = process(data, this.state);
    }

    public abrirRegistroDialogo() {
        const dialogRefRegistro = this.dialogService.open({
            appendTo: this.containerRegsitroRef,
            content: SocioTecnicoRegistrarComponent,
            minWidth: 400,
            maxWidth: 400,
            maxHeight: 700,
            title: 'REGISTRO',
            actions: [{ text: 'Cancelar' }, { text: 'Aceptar', primary: true }],
            preventAction: (ev, dialog) => {
                if (ev['text'] === 'Aceptar')
                    return !dialog.content.instance.editForm.valid;
                else
                    return false;
            }
        });

        const socioRegistro = dialogRefRegistro.content.instance;
        socioRegistro.socioEditar = this.socioEditar
        dialogRefRegistro.result.subscribe(r => {
            if (r['text'] == 'Aceptar') {
                this.guardarSocioTecnico(socioRegistro.editForm.value);
                dialogRefRegistro.close();
            }
            if (r['text'] == 'Cancelar') {
                dialogRefRegistro.close();
            }
        });
    }

    public editarSocio(item: SocioTecnico) {
        this.socioEditar = item;
        this.abrirRegistroDialogo();
    }

    public crearSocio(){
        this.socioEditar = null;
        this.abrirRegistroDialogo();
    }

    private guardarSocioTecnico(formulario) {
        if (!this.socioEditar) {
            this._socioTecnicoService.registrar(formulario).pipe(switchMap(() => {
                return this._socioTecnicoService.listarTodos();
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Socio Regsitrado");
                this.refrescarGrid(data);
            })
        } else {
            this._socioTecnicoService.modificar(formulario).pipe(switchMap(() => {
                return this._socioTecnicoService.listarTodos();
            })).subscribe(data => {
                this._notificarService.desactivarLoading();
                this._notificarService.mostrarMensajeExito("Socio Modificado");
                this.refrescarGrid(data);
            })
        }
    }


}
