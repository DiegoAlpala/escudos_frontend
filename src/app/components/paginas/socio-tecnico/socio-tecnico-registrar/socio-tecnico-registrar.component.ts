import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SocioTecnico } from 'src/app/_dominio/administracion/socioTecnico';
import { NotificarService } from 'src/app/_servicio/notificar.service';

@Component({
    selector: 'app-socio-tecnico-registrar',
    templateUrl: './socio-tecnico-registrar.component.html',
    styleUrls: ['./socio-tecnico-registrar.component.scss']
})
export class SocioTecnicoRegistrarComponent implements OnInit {

    public editForm: FormGroup = new FormGroup({
        id: new FormControl(0),
        codigoIdentificador: new FormControl('', Validators.required),
        nombre: new FormControl('', [Validators.required]),
        celular: new FormControl(''),
        telefono: new FormControl(''),
        correo: new FormControl(''),
        activo: new FormControl(1),
        nombreContacto: new FormControl(''),
        telefonoContacto: new FormControl(''),
        comentario:new FormControl(''),
        enlace: new FormControl(''),
    });

    private socioEditar: SocioTecnico;

    constructor(
        private _notificarService: NotificarService,
    ) { }

    ngOnInit(): void {
        this.cargarDatos();
    }

    private cargarDatos() {
        if (this.socioEditar) {
            this.editForm.controls['id'].setValue(this.socioEditar.id);
            this.editForm.controls['codigoIdentificador'].setValue(this.socioEditar.codigoIdentificador);
            this.editForm.controls['nombre'].setValue(this.socioEditar.nombre);
            this.editForm.controls['celular'].setValue(this.socioEditar.celular);
            this.editForm.controls['telefono'].setValue(this.socioEditar.telefono);
            this.editForm.controls['correo'].setValue(this.socioEditar.correo);
            this.editForm.controls['activo'].setValue(this.socioEditar.activo);
            this.editForm.controls['nombreContacto'].setValue(this.socioEditar.nombreContacto);
            this.editForm.controls['telefonoContacto'].setValue(this.socioEditar.telefonoContacto);
            this.editForm.controls['comentario'].setValue(this.socioEditar.comentario);
            this.editForm.controls['enlace'].setValue(this.socioEditar.enlace);
        }
    }

    public esEdicion() {
        return this.socioEditar ? true : false;
    }

}
