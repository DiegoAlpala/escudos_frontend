import { Component } from '@angular/core';
import { Menu } from 'src/app/_dominio/sistema/menu';
import { LoginService } from 'src/app/_servicio/login.service';
import { MenuService } from 'src/app/_servicio/sistema/menu.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    public projectName = 'Kendo UI for Angular';
    public usuarioNombreCompleto: string;
    //public menus: any[] = menus;
    menus: Menu[];

    public logoHeader = "assets/logo-header.JPG";

    constructor(private _menuService: MenuService,
        public _loginService: LoginService,
    ) { }

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this._menuService.menuCambio.subscribe(data => {
            this.menus = data;
        });

        this._menuService.usuarioNombreCompleto.subscribe(data => {
            this.usuarioNombreCompleto = data;
        });
    }

}



