import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { ActiveBadgeComponent } from './active-badge/active-badge.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { LabelModule } from '@progress/kendo-angular-label';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EstadoBadgeComponent } from './estado-badge/estado-badge.component';
//import { GarantiaTipoBadgeComponent } from './garantia-tipo-badge/garantia-tipo-badge.component';



@NgModule({
    declarations: [
       /*  ActiveBadgeComponent,
        GarantiaTipoBadgeComponent */

    ],
    imports: [
        CommonModule,
        GridModule,
        LayoutModule,
        ButtonsModule,
        LabelModule,
        InputsModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    exports: [
       /*  ActiveBadgeComponent,
        GarantiaTipoBadgeComponent */
    ]
})
export class CommonsModule { }
