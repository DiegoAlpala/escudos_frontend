import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-garantia-tipo-badge',
    templateUrl: './garantia-tipo-badge.component.html',
    styleUrls: ['./garantia-tipo-badge.component.scss']
})
export class GarantiaTipoBadgeComponent implements OnInit {

    @Input()
    tipoGarantia: string;

    constructor() { }

    ngOnInit(): void {
    }

    public colorBadge() {
        switch (this.tipoGarantia) {
            case "ORO":
                return ['badge-warning'];
            case "PLATA":
                return ['badge-light'];
            case "BRONCE":
                return ['badge-secondary'];
            case "PLATINO":
                return ['badge-info'];
            default:
                return [''];
        }
    }


}
