import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-estado-badge',
    templateUrl: './estado-badge.component.html',
    styleUrls: ['./estado-badge.component.scss']
})
export class EstadoBadgeComponent implements OnInit {

    @Input()
    estado: string;

    constructor() { }

    ngOnInit(): void {
    }

    public colorBadge() {
        switch (this.estado) {
            case "PENDIENTE":
                return ['badge-success'];
            case "ASIGNADO":
                return ['badge-danger'];
            default:
                return ['badge-ligth'];
        }
    }


}
