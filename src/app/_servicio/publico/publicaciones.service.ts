import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Obra } from "src/app/_dominio/administracion/obra";
import { environment } from "src/environments/environment";
import { NotificarService } from "../notificar.service";

@Injectable({
    providedIn: 'root'
})
export class PublicacionesService {
    url: string = `${environment.HOST}/publicaciones`;

    constructor(
        private http: HttpClient,
        private _notificarService: NotificarService
    ) { }

    listarPorSerie(serie: string) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Obra>(`${this.url}/informacionQR/${serie}`);
    }


}