import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ConsultaQR } from 'src/app/_dominio/administracion/consultaQR_DTO';
import { EscudoGarantia } from 'src/app/_dominio/administracion/escudoGarantia';
import { ImpermeabilizacionSistema } from 'src/app/_dominio/administracion/impermeabilizacionSistema';
import { Obra } from 'src/app/_dominio/administracion/obra';
import { environment } from 'src/environments/environment';
import { NotificarService } from '../notificar.service';

@Injectable({
    providedIn: 'root'
})
export class ObraService {

    url: string = `${environment.HOST}/obras`;

    obrasCambio = new Subject<Obra[]>();
    obraActualizado = new Subject<Obra>();

    constructor(
        private http: HttpClient,
        private _notificarService: NotificarService
    ) { }

    listarTodos() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Obra[]>(this.url)
    }

    listarPorId(idObra: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Obra>(`${this.url}/${idObra}`);
    }

    registrar(obra: Obra) {
        this._notificarService.loadingCambio.next(true);
        return this.http.post<Obra>(this.url, obra);
    }

    modificar(obra: Obra) {
        this._notificarService.loadingCambio.next(true);
        return this.http.put<Obra>(this.url, obra);
    }

    listarPorSerie(serie: string) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Obra>(`${this.url}/serie/${serie}`);
    }

    obtenerCatalogoRegiones() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<any[]>(`${this.url}/regiones`);
    }

    obtenerCatalogoTipoGarantia() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<any[]>(`${this.url}/tipoGarantias`);
    }

    generarQR(id: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get(`${this.url}/generarQR/${id}`, {
            responseType: 'blob'
        });
    }

    asignarGarantias(socioId: number, tipoGarantia: string, cantidad: number) {
        let dto = { 'socio': socioId, 'tipoGarantia': tipoGarantia, 'cantidad': cantidad };
        this._notificarService.loadingCambio.next(true);
        return this.http.post<any[]>(`${this.url}/asignarGarantias`, dto);
    }

    agregarSistema(sistema: ImpermeabilizacionSistema, idObra: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.post<ImpermeabilizacionSistema[]>(`${this.url}/agregarSistema/${idObra}`, sistema);
    }

    agregarEscudo(escudo: EscudoGarantia, idObra: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.post<EscudoGarantia[]>(`${this.url}/agregarEscudo/${idObra}`, escudo);
    }

    consultarQRTodos() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<ConsultaQR[]>(`${this.url}/consultaTodosQR`)
    }

    obtenerCatalogoEstadoObra() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<any[]>(`${this.url}/estado`);
    }

    consultarObras(page: number, size: number, consulta: any) {
        this._notificarService.activarLoading();
        let urlFinal = `${this.url}/consulta/?page=${page}&size=${size}`;
        return this.http.post<any>(urlFinal, consulta);
    }

    consultarTodo(consulta: any) {
        this._notificarService.activarLoading();
        let urlFinal = `${this.url}/consulta/todo`;
        return this.http.post<any>(urlFinal, consulta);
    }

    generarReporteObras(consulta: any) {
        this._notificarService.loadingCambio.next(true);
        return this.http.post(`${this.url}/consulta/todo`, consulta, {
            responseType: 'blob'
        });
    }

    getPosition(): Promise<any> {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(resp => {
                resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
            },
                err => {
                    reject(err);
                });
        });
    }

    generarReporteEscudoQr(id: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get(`${this.url}/reporteObraEscudoQr/${id}`, {
            responseType: 'blob'
        });
    }
}
