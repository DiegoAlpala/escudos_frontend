import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MaterialCatalogo } from 'src/app/_dominio/administracion/material-catalogo';
import { environment } from 'src/environments/environment';
import { NotificarService } from '../notificar.service';

@Injectable({
	providedIn: 'root'
})
export class MaterialCatalogoService {

    maerialCatalogoCambio = new Subject<MaterialCatalogo[]>();
	url: string = `${environment.HOST}/materialesCatalogo`;

	constructor(
		private http: HttpClient,
		private _notificarService: NotificarService
	) { }

	registrar(material: MaterialCatalogo) {
		this._notificarService.loadingCambio.next(true);
		return this.http.post<MaterialCatalogo>(this.url, material);
	}

    modificar(material: MaterialCatalogo) {
		this._notificarService.loadingCambio.next(true);
		return this.http.put<MaterialCatalogo>(this.url, material);
	}

	listarTodos(){
        this._notificarService.loadingCambio.next(true);
		return this.http.get<MaterialCatalogo[]>(this.url);
    }


	listarActivos(){
        this._notificarService.loadingCambio.next(true);
		return this.http.get<MaterialCatalogo[]>(`${this.url}/activos`);
    }
}
