import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SocioTecnico } from 'src/app/_dominio/administracion/socioTecnico';
import { environment } from 'src/environments/environment';
import { NotificarService } from '../notificar.service';

@Injectable({
    providedIn: 'root'
})
export class SocioTecnicoService {

    url: string = `${environment.HOST}/socios`;

    socioCambio = new Subject<SocioTecnico[]>();

    constructor(
        private http: HttpClient,
        private _notificarService: NotificarService
    ) { }

    listarTodos() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<SocioTecnico[]>(this.url)
    }

    listarTodosActivos() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<SocioTecnico[]>(`${this.url}/activos`)
    }

    listarPorId(idSocio: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<SocioTecnico>(`${this.url}/${idSocio}`);
    }

    registrar(socio: SocioTecnico) {
        this._notificarService.loadingCambio.next(true);
        return this.http.post<SocioTecnico>(this.url, socio);
    }

    modificar(socio: SocioTecnico) {
        this._notificarService.loadingCambio.next(true);
        return this.http.put<SocioTecnico>(this.url, socio);
    }
}
