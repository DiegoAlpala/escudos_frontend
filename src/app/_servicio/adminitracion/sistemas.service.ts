import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ImpermeabilizacionSistema } from 'src/app/_dominio/administracion/impermeabilizacionSistema';
import { Material } from 'src/app/_dominio/administracion/material';
import { environment } from 'src/environments/environment';
import { NotificarService } from '../notificar.service';

@Injectable({
    providedIn: 'root'
})
export class SistemasService {

    url: string = `${environment.HOST}/sistemas`;
    constructor(
        private http: HttpClient,
        private _notificarService: NotificarService
    ) { }

    modificar(sistema: ImpermeabilizacionSistema) {
        this._notificarService.loadingCambio.next(true);
        return this.http.put<ImpermeabilizacionSistema>(this.url, sistema);
    }

    agregarMaterial(material: Material, idSistema: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.post<Material[]>(`${this.url}/agregarMaterial/${idSistema}`, material);
    }

    eliminarSistema(id: number){
        this._notificarService.loadingCambio.next(true);
        return this.http.delete<boolean>(`${this.url}/${id}`);
    }
}
