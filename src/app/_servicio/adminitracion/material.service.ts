import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Material } from 'src/app/_dominio/administracion/material';
import { environment } from 'src/environments/environment';
import { NotificarService } from '../notificar.service';

@Injectable({
	providedIn: 'root'
})
export class MaterialService {

	url: string = `${environment.HOST}/materiales`;

	constructor(
		private http: HttpClient,
		private _notificarService: NotificarService
	) { }

	modificar(material: Material) {
		this._notificarService.loadingCambio.next(true);
		return this.http.put<Material>(this.url, material);
	}

	eliminarMaterial(id: number){
        this._notificarService.loadingCambio.next(true);
        return this.http.delete<boolean>(`${this.url}/${id}`);
    }

    obtenerProductos(){
        return this.http.get<any[]>("../../assets/productos.json");
    }

    obtenerUnidades(){
        return this.http.get<any[]>("../../assets/unidades.json");
    }
}
