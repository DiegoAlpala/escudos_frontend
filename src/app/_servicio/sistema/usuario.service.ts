import { Usuario } from './../../_dominio/sistema/usuario';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { NotificarService } from '../notificar.service';

@Injectable({
    providedIn: 'root'
})
export class UsuarioService {

    url: string = `${environment.HOST}/usuarios`;

    usuariosCambio = new Subject<Usuario[]>();

    constructor(
        private http: HttpClient,
        private _notificarService: NotificarService
    ) { }

    listarTodos() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Usuario[]>(this.url)
    }

    listarPorId(id: number) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Usuario>(`${this.url}/${id}`);
    }

    registrar(usuairo: Usuario) {
        this._notificarService.loadingCambio.next(true);
        return this.http.post<Usuario>(this.url, usuairo);
    }

    modificar(usuario: Usuario) {
        this._notificarService.loadingCambio.next(true);
        return this.http.put<Usuario>(this.url, usuario);
    }

    listarTipoUsuario() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<any[]>(`${this.url}/tipos`);
    }

    cambiarContrasena(idUsuario: number, contrasena: string) {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<boolean>(`${this.url}/cambiarContrasena/${idUsuario}/${contrasena}`);
    }

}
