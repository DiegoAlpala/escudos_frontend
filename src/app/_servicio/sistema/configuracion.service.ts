import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotificarService } from '../notificar.service';
import { Configuracion } from 'src/app/_dominio/sistema/configuracion';

@Injectable({
    providedIn: 'root'
})
export class ConfiguracionService {
    menuCambio = new Subject<Configuracion[]>();
    usuarioNombreCompleto = new Subject<string>();

    url: string = `${environment.HOST}/configuraciones`;

    constructor(
        private http: HttpClient,
        private _notificarService: NotificarService
    ) { }

    listarTodos() {
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Configuracion[]>(this.url)
    }

    modificar(configuracion: Configuracion) {
        return this.http.put<Configuracion>(this.url, configuracion);
    }


}
