import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Menu } from '../../_dominio/sistema/menu';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Rol } from 'src/app/_dominio/sistema/rol';
import { NotificarService } from '../notificar.service';

@Injectable({
    providedIn: 'root'
})
export class MenuService {
    menuCambio = new Subject<Menu[]>();
    usuarioNombreCompleto = new Subject<string>();

    url: string = `${environment.HOST}/roles`;

    constructor(
        private http: HttpClient,
        private _notificarService: NotificarService
        ) { }

    listarPorRol() {
        let token = sessionStorage.getItem(environment.TOKEN_NAME);
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(token);
        return this.http.get<Menu[]>(`${this.url}/menuPorRol/${decodedToken.authorities[0]}`);
    }

    listarTodosRoles(){
        this._notificarService.loadingCambio.next(true);
        return this.http.get<Rol[]>(this.url)
    }

}
